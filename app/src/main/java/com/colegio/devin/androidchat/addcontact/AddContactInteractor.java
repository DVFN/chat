package com.colegio.devin.androidchat.addcontact;

/**
 * Created by devin.
 */
public interface AddContactInteractor {
    void execute(String email);
}
