package com.colegio.devin.androidchat.addcontact;

import com.colegio.devin.androidchat.addcontact.events.AddContactEvent;

/**
 * Created by devin.
 */
public interface AddContactPresenter {
    void onShow();
    void onDestroy();

    void addContact(String email);
    void onEventMainThread(AddContactEvent event);
}
