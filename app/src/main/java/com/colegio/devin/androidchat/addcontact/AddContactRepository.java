package com.colegio.devin.androidchat.addcontact;

/**
 * Created by devin.
 */
public interface AddContactRepository {
    void addContact(String email);
}
