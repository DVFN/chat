package com.colegio.devin.androidchat.addcontact.events;

/**
 * Created by devin.
 */
public class AddContactEvent {
    boolean error = false;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
