package com.colegio.devin.androidchat.addcontact.ui;

/**
 * Created by devin.
 */
public interface AddContactView {
    void showInput();
    void hideInput();
    void showProgress();
    void hideProgress();

    void contactAdded();
    void contactNotAdded();
}
