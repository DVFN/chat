package com.colegio.devin.androidchat.chat;

/**
 * Created by avalo.
 */
public interface ChatSessionInteractor {
    void changeConnectionStatus(boolean online);
}
