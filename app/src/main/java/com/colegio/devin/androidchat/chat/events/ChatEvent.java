package com.colegio.devin.androidchat.chat.events;

import com.colegio.devin.androidchat.entities.ChatMessage;

/**
 * Created by avalo.
 */
public class ChatEvent {
    private ChatMessage message;

    public ChatMessage getMessage() {
        return message;
    }

    public void setMessage(ChatMessage message) {
        this.message = message;
    }
}
