package com.colegio.devin.androidchat.chat.ui;

import com.colegio.devin.androidchat.entities.ChatMessage;

/**
 * Created by avalo.
 */
public interface ChatView {
    void onMessageReceived(ChatMessage msg);
}
