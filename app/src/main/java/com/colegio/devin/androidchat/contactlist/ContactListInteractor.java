package com.colegio.devin.androidchat.contactlist;

/**
 * Created by devin.
 */
public interface ContactListInteractor {
    void subscribe();
    void unsubscribe();
    void destroyListener();
    void removeContact(String email);
}
