package com.colegio.devin.androidchat.contactlist;

/**
 * Created by devin.
 */
public interface ContactListSessionInteractor {
    void signOff();
    String getCurrentUserEmail();
    void changeConnectionStatus(boolean online);
}
