package com.colegio.devin.androidchat.contactlist.ui;

import com.colegio.devin.androidchat.entities.User;

/**
 * Created by devin.
 */
public interface ContactListView {
    void onContactAdded(User user);
    void onContactChanged(User user);
    void onContactRemoved(User user);
}
