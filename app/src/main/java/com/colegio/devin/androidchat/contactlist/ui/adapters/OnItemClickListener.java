package com.colegio.devin.androidchat.contactlist.ui.adapters;

import com.colegio.devin.androidchat.entities.User;

/**
 * Created by devin.
 */
public interface OnItemClickListener {
    void onItemClick(User user);
    void onItemLongClick(User user);
}
