package com.colegio.devin.androidchat.lib;

/**
 * Created by devin.
 */
public interface EventBus {
    void register(Object subscriber);
    void unregister(Object subscriber);
    void post(Object event);
}
