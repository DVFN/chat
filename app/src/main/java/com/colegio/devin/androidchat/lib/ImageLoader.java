package com.colegio.devin.androidchat.lib;

import android.widget.ImageView;

/**
 * Created by devin.
 */
public interface ImageLoader {
    void load(ImageView imgAvatar, String url);
}
