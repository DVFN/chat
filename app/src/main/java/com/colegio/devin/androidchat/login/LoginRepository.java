package com.colegio.devin.androidchat.login;

/**
 * Created by devin.
 */
public interface LoginRepository {
    void signUp(String email, String password);
    void signIn(String email, String password);
    void checkSession();
}
